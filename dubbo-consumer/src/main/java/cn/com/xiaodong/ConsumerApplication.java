package cn.com.xiaodong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * 生产者项目启动类
 * auth maxiaodong time 2018/3/12.
 */
@SpringBootApplication
@ImportResource(value = {"classpath:dubbo-consumer.xml"})
public class ConsumerApplication {

    /**
     * 项目启动类
     * @param args
     */
    public static void main(String[] args) {
        //
        SpringApplication.run(ConsumerApplication.class, args);
    }
}

package cn.com.xiaodong.controller;


import cn.com.xiaodong.vos.UserVo;
import com.dubbo.provider.api.UserService;
import com.dubbo.provider.dto.UserDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 用户信息业务控制类
 * auth maxiaodong time 2018/3/12.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @RequestMapping("/getById")
    public UserVo getUser(Long userId){
        UserDTO userDTO = userService.getUserVoByUserId(userId);

        // 封装返回数据
        UserVo userVo = new UserVo();
        userVo.setUserName(userDTO.getUserName());
        return userVo;
    }
}

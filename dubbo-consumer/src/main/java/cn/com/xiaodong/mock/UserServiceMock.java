package cn.com.xiaodong.mock;

import com.dubbo.provider.api.UserService;
import com.dubbo.provider.dto.UserDTO;


public class UserServiceMock implements UserService {

    public UserServiceMock(){

    }

    @Override
    public UserDTO getUserVoByUserId(Long userId) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserName("我是mock数据");
        return userDTO;
    }
}

package com.dubbo.provider.api;

import java.util.List;

public interface MenuService {

    /**
     * 菜单--列表
     *
     * @return
     */
    List<String> getListMenu();
}

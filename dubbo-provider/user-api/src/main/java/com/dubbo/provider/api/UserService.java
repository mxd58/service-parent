package com.dubbo.provider.api;

import com.dubbo.provider.dto.UserDTO;

public interface UserService {

    /**
     * 获取--用户信息
     *
     * @param userId
     * @return
     */
    UserDTO getUserVoByUserId(Long userId);
}

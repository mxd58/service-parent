package com.dubbo.provider.dto;


import java.io.Serializable;

/**
 * auth maxiaodong time 2018/3/25.
 */
public class UserDTO implements Serializable {

    /**
     * 用户名
     */
    public String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}

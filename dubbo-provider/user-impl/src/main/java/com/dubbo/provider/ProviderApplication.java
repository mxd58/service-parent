package com.dubbo.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * 项目运行服务的启动类
 * auth maxiaodong time 2018/3/12.
 */
@SpringBootApplication
@ImportResource(value = {"classpath:dubbo-provider.xml"})
public class ProviderApplication {

    /**
     * 项目启动类
     * @param args
     */
    public static void main(String[] args) {
        //启动方法
        SpringApplication.run(ProviderApplication.class, args);
    }

}

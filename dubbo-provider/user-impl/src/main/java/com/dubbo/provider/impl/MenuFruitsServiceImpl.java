package com.dubbo.provider.impl;

import com.dubbo.provider.api.MenuService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuFruitsServiceImpl implements MenuService {

    /**
     * 水果--菜单
     *
     * @return
     */
    @Override
    public List<String> getListMenu() {
        List<String> fru = new ArrayList<>();
        fru.add("葡萄");
        fru.add("桃子");
        return fru;
    }
}

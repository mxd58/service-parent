package com.dubbo.provider.impl;

import com.dubbo.provider.api.MenuService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuMobileServiceImpl implements MenuService {

    /**
     * 水果--菜单
     *
     * @return
     */
    @Override
    public List<String> getListMenu() {
        List<String> fru = new ArrayList<>();
        fru.add("苹果");
        fru.add("三星");
        fru.add("华为");
        return fru;
    }
}

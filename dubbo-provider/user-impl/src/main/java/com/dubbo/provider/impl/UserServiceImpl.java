package com.dubbo.provider.impl;


import com.dubbo.provider.api.UserService;
import com.dubbo.provider.dto.UserDTO;
import com.dubbo.provider.entity.UserEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * auth maxiaodong time 2018/3/11.
 */
@Service
public class UserServiceImpl implements UserService {

    @Value("${server.port}")
    private String port;

    @Override
    public UserDTO getUserVoByUserId(Long userId) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName("我来自:" + port + ",我叫马云老头");

        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userEntity,userDTO);
        return userDTO;
    }
}

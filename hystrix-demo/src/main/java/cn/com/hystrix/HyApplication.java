package cn.com.hystrix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * auth maxiaodong time 2018/9/15.
 */
@SpringBootApplication
public class HyApplication {

    public static void main(String[] args) {
        //
        SpringApplication.run(HyApplication.class, args);
    }

}

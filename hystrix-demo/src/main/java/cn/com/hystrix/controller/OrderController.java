package cn.com.hystrix.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * auth maxiaodong time 2018/9/15.
 *
 * 控制层：
 */
@RestController
public class OrderController {

    @GetMapping(value = "/")
    public String index(){
        return "欢迎来动【豪猪】监控系统....";
    }

    @GetMapping(value = "/getOrderPageList")
    /*@HystrixCommand(
            fallbackMethod = "getOrderPageListFallback",
            threadPoolProperties = {  //10个核心线程池,超过20个的队列外的请求被拒绝; 当一切都是正常的时候，线程池一般仅会有1到2个线程激活来提供服务
                    @HystrixProperty(name = "coreSize", value = "10"),
                    @HystrixProperty(name = "maxQueueSize", value = "100"),
                    @HystrixProperty(name = "queueSizeRejectionThreshold", value = "20")},
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000"), //命令执行超时时间
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "2"), //若干10s一个窗口内失败三次, 则达到触发熔断的最少请求量
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "30000") //断路30s后尝试执行, 默认为5s
            })*/
    public String getOrderPageList(String orderNo) {
        System.out.println("线程名:"+ Thread.currentThread().getName() + ","+"订单号：" + orderNo);
        return "处理成功";
    }

    public String getOrderPageListFallback(String orderNo){
        System.out.println("执行降级策略:" + orderNo);
        return "执行降级策略";
    }
}

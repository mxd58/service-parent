package cn.com.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MqApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqApplication.class,args);
        System.out.println("======启动成功.....");
    }
}

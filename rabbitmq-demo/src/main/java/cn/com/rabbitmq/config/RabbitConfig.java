package cn.com.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    /**
     * 声明队列--是幂等的，只有在它不存在的情况下才会创建它。
     *
     * @return
     */
    @Bean
    public Queue helloQueue() {
        return new Queue("user");
    }

    @Bean
    public Queue userQueue() {
        return new Queue("user_name");
    }

    @Bean
    public Queue manyUserQueue() {
        return new Queue("many_user_name");
    }

    // 创建一个交换机,这个交换机，
    @Bean
    public DirectExchange crmExchange() {
        return new DirectExchange("user_exchange");
    }

    @Bean
    public Queue routingUserQueue01() {
        return new Queue("routing_user_name_one");
    }

    @Bean
    public Queue routingUserQueue02() {
        return new Queue("routing_user_name_two");
    }

    /**
     * 绑定exchange & queue & routeKey.
     *
     * @return
     */
    @Bean
    public Binding bindingExchange01() {
        return BindingBuilder.bind(routingUserQueue01()).to(crmExchange()).with("user-routing-key");
    }

    @Bean
    public Binding bindingExchange02() {
        return BindingBuilder.bind(routingUserQueue02()).to(crmExchange()).with("user-routing-key");
    }

    /**创建一个Topic交换机,这个交换机，测试主题交换机**/
    @Bean
    public TopicExchange crmTopicExchange() {
        return new TopicExchange("test_topic_exchange");
    }

    /**
     * 创建队列
     * @return
     */
    @Bean
    public Queue topicQueue01() {
        return new Queue("topic_queue_one");
    }

    @Bean
    public Queue topicQueue02() {
        return new Queue("topic_queue_two");
    }

    /**队列Q01对所有的橙色动物感兴趣**/
    @Bean
    public Binding bindingTopicExchange01() {
        return BindingBuilder.bind(topicQueue01()).to(crmTopicExchange()).with("*.orange.*");
    }

    /**Q2想听听有关兔子的一切，以及关于lazy动物的一切**/
    @Bean
    public Binding bindingTopicExchange02() {
        return BindingBuilder.bind(topicQueue02()).to(crmTopicExchange()).with("*.*.rabbit");
    }

    @Bean
    public Binding bindingTopicExchange03() {
        return BindingBuilder.bind(topicQueue02()).to(crmTopicExchange()).with("lazy.＃");
    }
}

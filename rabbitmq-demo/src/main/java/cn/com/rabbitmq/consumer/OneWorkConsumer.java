package cn.com.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.concurrent.TimeUnit;

@Component
public class OneWorkConsumer {

    @RabbitHandler
    @RabbitListener(queues = "user_name")
    public void process(String userName) throws InterruptedException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        TimeUnit.SECONDS.sleep(4);
        System.out.println(" [X] Received '" + userName + "'");
        stopWatch.stop();
        long totalTimeMillis = stopWatch.getTotalTimeMillis();
        System.out.println("====执行任务的时间：" + totalTimeMillis);
    }

    @RabbitHandler
    @RabbitListener(queues = "user_name")
    public void process02(String userName) {
        System.out.println(" [Z] Received '" + userName + "'");
    }
}

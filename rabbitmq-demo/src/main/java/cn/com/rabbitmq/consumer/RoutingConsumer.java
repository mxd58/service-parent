package cn.com.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RoutingConsumer {

    @RabbitHandler
    @RabbitListener(queues = "routing_user_name_one")
    public void process01(String hello) {
        System.out.println("HelloSubscriber01 : " + hello);
    }

    @RabbitHandler
    @RabbitListener(queues = "routing_user_name_two")
    public void process02(String hello) {
        System.out.println("HelloSubscriber02 : " + hello);
    }
}

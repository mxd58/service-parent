package cn.com.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class TopicConsumer {

    @RabbitHandler
    @RabbitListener(queues = "topic_queue_one")
    public void process01(String hello) {
        System.out.println("topic_queue_one==> : " + hello);
    }

    @RabbitHandler
    @RabbitListener(queues = "topic_queue_two")
    public void process02(String hello) {
        System.out.println("topic_queue_two==> : " + hello);
    }
}

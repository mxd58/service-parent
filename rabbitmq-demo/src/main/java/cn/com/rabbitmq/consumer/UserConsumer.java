package cn.com.rabbitmq.consumer;

import cn.com.rabbitmq.message.UserVO;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "user")
public class UserConsumer {

    @RabbitHandler
    public void process(UserVO userVo) {
        System.out.println("单对单接收参数 : " + userVo.getUserName());
    }
}

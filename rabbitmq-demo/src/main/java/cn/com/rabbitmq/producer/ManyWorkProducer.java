package cn.com.rabbitmq.producer;

import cn.com.rabbitmq.utils.RabbitUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 主要测试：多生产者，多消费者情况下，任务的队列的分发情况。
 * 测试结果：一条消息不会被两个消费同时接受到。
 *
 */
@Component
public class ManyWorkProducer {

    @Autowired
    private RabbitUtil rabbitUtil;

    //@Scheduled(fixedDelay = 3000, initialDelay = 500)
    public void sendUser01() {
        String dots = "";
        for (int i=0;i<5;i++){
            dots += ".";
            String userName = "马晓东ONE" + dots + dots.length();
            rabbitUtil.convertAndSend("many_user_name",userName);
            System.out.println(" [oneP] Sent '" +  userName + "'");
        }
    }

    //@Scheduled(fixedDelay = 3000, initialDelay = 500)
    public void sendUse02r() {
        String dots = "";
        for (int i=0;i<5;i++){
            dots += ".";
            String userName = "马晓东TWO" + dots + dots.length();
            rabbitUtil.convertAndSend("many_user_name",userName);
            System.out.println(" [twoP] Sent '" +  userName + "'");
        }
    }
}

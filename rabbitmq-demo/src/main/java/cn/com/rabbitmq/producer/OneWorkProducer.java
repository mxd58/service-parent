package cn.com.rabbitmq.producer;

import cn.com.rabbitmq.utils.RabbitUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


/**
 * 主要测试--一个生产者多个消费者。测试结果，一条消息不会被两个消费同时接受到。
 */
@Component
public class OneWorkProducer {

    @Autowired
    private RabbitUtil rabbitUtil;

    //@Scheduled(fixedDelay = 100000, initialDelay = 500)
    public void sendUser() {
        String dots = "";
        for (int i=0;i<10;i++){
            dots += ".";
            String userName = "马晓东" + dots + dots.length();
            rabbitUtil.convertAndSend("user_name",userName);
            System.out.println(" [p] Sent '" +  userName + "'");
        }
    }
}

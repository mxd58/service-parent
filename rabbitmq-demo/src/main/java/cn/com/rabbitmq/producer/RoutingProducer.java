package cn.com.rabbitmq.producer;

import cn.com.rabbitmq.config.RabbitConfig;
import cn.com.rabbitmq.utils.RabbitUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 主要测试：队列的绑定的与路由规则
 *
 */
@Component
public class RoutingProducer {
    @Autowired
    private RabbitUtil rabbitUtil;

    @Autowired
    private RabbitConfig rabbitConfig;

    //@Scheduled(fixedDelay = 3000, initialDelay = 500)
    public void hello() {
        String context = "hello " + new Date();
        System.out.println("Hello Publisher : " + context);
        this.rabbitUtil.convertAndSend("user_exchange", "user-routing-key", context);
    }
}

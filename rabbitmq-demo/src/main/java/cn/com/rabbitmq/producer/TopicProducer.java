package cn.com.rabbitmq.producer;

import cn.com.rabbitmq.utils.RabbitUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TopicProducer {

    @Autowired
    private RabbitUtil rabbitUtil;

    @Scheduled(fixedDelay = 3000, initialDelay = 500)
    public void hello01() {
        String context = "hello " + new Date();
        System.out.println("HelloPublisher : " + context);
        this.rabbitUtil.convertAndSend("test_topic_exchange", "quick.orange.rabbit", context);
    }
}

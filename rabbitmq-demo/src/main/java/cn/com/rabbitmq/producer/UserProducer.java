package cn.com.rabbitmq.producer;

import cn.com.rabbitmq.message.UserVO;
import cn.com.rabbitmq.utils.RabbitUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 这个只是模仿，最简单的队列模式。一个生产者对应一个消费者
 */
@Component
public class UserProducer {

    @Autowired
    private RabbitUtil rabbitUtil;

    public void sendUser() {
        UserVO userVO = new UserVO();
        userVO.setAge("28");
        userVO.setSex("男");
        userVO.setUserName("马晓东");
        rabbitUtil.convertAndSend("user",userVO);
    }
}

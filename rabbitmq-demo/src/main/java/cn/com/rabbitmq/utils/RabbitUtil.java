package cn.com.rabbitmq.utils;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitUtil {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    /**
     * 生产者发送--消息到指定队列
     *
     * @param routingKey
     * @param message
     */
    public void send(String routingKey, Message message) {
        rabbitTemplate.send(routingKey,message);
    }

    /**
     * 生产者--使用MQ默认交换机将消息发送到指定队列
     *
     * @param routingKey
     * @param object
     */
    public void convertAndSend(String routingKey,Object object) {
        rabbitTemplate.convertAndSend(routingKey,object);
    }

    /**
     *
     *
     * @param exchange
     * @param routingKey
     * @param object
     */
    public void convertAndSend(String exchange, String routingKey, Object object){
        rabbitTemplate.convertAndSend(exchange,routingKey,object);
    }
}

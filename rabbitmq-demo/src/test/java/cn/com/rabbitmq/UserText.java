package cn.com.rabbitmq;

import cn.com.rabbitmq.producer.RoutingProducer;
import cn.com.rabbitmq.producer.UserProducer;
import cn.com.rabbitmq.producer.OneWorkProducer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserText {

    @Autowired
    private UserProducer userProducer;

    @Autowired
    private OneWorkProducer workProducer;

    @Autowired
    private RoutingProducer routingProducer;

    @Test
    public void sss01(){
        routingProducer.hello();
    }
}

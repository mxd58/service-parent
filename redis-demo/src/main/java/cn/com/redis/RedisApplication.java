package cn.com.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * auth maxiaodong time 2018/9/9.
 */
@SpringBootApplication
@RestController
public class RedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class,args);
    }

    @GetMapping(value = "/index")
    public String index(){
       return "欢迎使用Redis-Demo程序";
    }
}

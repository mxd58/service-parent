package cn.com.redis.bean;

import java.io.Serializable;

/**
 * auth maxiaodong time 2018/9/9.
 *
 */
public class UserVO implements Serializable {

    private static final long serialVersionUID = -3991636847844839983L;

    /**用户名**/
    private String userName;

    /**年龄**/
    private String age;

    /**性别**/
    private String sex;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

}

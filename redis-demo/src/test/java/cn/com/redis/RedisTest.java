package cn.com.redis;

import cn.com.redis.utils.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * auth maxiaodong time 2018/9/9.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RedisTest {

    @Autowired
    private RedisUtil redisUtil;

    @Test
    public void getString() {
        System.out.println(redisUtil.get("name"));
    }

    @Test
    public void setString(){
        redisUtil.set("name2","maxiaodong");
    }

    @Test
    public void delString(){
        redisUtil.del("name");
    }

}

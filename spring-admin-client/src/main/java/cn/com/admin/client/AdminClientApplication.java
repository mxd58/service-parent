package cn.com.admin.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * auth maxiaodong time 2018/9/16.
 */
@SpringBootApplication
public class AdminClientApplication {

    public static void main(String[] args) {
        //
        SpringApplication.run(AdminClientApplication.class,args);
    }
}

package cn.com.websocket.controller;

import cn.com.websocket.server.WebSocketServer;
import cn.com.websocket.vo.MessageVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/checkcenter")
public class WebSocketController {

    //页面请求
    @GetMapping("/socket/{cid}")
    public ModelAndView socket(@PathVariable String cid) {
        ModelAndView mav=new ModelAndView("/socket");
        mav.addObject("cid", cid);
        return mav;
    }

    //推送数据接口
    @RequestMapping("/socket/push")
    @ResponseBody
    public void pushToWeb(@RequestBody MessageVO message) {
        WebSocketServer.sendInfo(message);
    }
}

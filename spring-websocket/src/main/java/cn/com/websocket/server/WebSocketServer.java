package cn.com.websocket.server;

import cn.com.websocket.vo.MessageVO;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint("/websocket/{sid}")
@Component
public class WebSocketServer {

    /**
     * 日志类
     **/
    private static final Logger logger = LoggerFactory.getLogger(WebSocketServer.class);

    /**
     * 在线人数
     */
    public static int onlineCount = 0;

    /**
     * 以用户的sid，WebSocket为对象保存起来，要保证sid的唯一性
     */
    private static Map<String, WebSocketServer> clients = new ConcurrentHashMap<>();

    /**
     * 会话
     */
    private Session session;

    /**
     *
     */
    private String sid = "";

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("sid") String sid) {
        this.session = session;
        clients.put(sid, this); // 把自己的信息加入到map当中去
        addOnlineCount(); // 在线数加1
        logger.info("有新窗口开始监听:[{}],当前在线人数为[{}]",sid,getOnlineCount());
        this.sid = sid;

        // 向所有人推送，我已上线
        MessageVO messageVO = new MessageVO();
        messageVO.setToSid(null);
        messageVO.setMessageType(1);
        messageVO.setMessage("上线通知");
        messageVO.setFromSid(sid);
        sendInfo(messageVO);
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        logger.info("客户端消息:" + message);
        logger.info("客户端ID是:" + session.getId());
        MessageVO messageVO = JSON.parseObject(message, new TypeReference<>() {});
        sendInfo(messageVO);
    }

    @OnError
    public void onError(Session session, Throwable error) {
        logger.info("服务端错误:{}", error.getMessage());
        logger.info("客户端ID是:{}", session.getId());
    }

    /**
     * 客户端下线
     */
    @OnClose
    public void onClose() {
        subOnlineCount();
        clients.remove(this.sid);
        // 发布下线同通知
        MessageVO messageVO = new MessageVO();
        messageVO.setFromSid(this.sid);
        messageVO.setMessage("下线通知");
        messageVO.setMessageType(2);
        messageVO.setToSid(null);
        sendInfo(messageVO);
        logger.info("有连接关闭！当前在线人数" + onlineCount);
    }

    /**
     * 实现服务器主动推送
     */
    public void sendMessage(MessageVO message) throws IOException {
        this.session.getBasicRemote().sendText(JSON.toJSONString(message));
    }

    /**
     * 群发或者给指定人发
     *
     * @param message
     * @throws IOException
     */
    public static void sendInfo(MessageVO message) {
        String toSid = message.getToSid();
        for (WebSocketServer item : clients.values()) {
            try {
                //这里可以设定只推送给这个sid的，为null则全部推送
                if (toSid == null || "".equals(toSid)) {
                    item.sendMessage(message);
                } else if (item.sid.equals(toSid)) {
                    logger.info("发送给指定窗口:[{}]" + toSid);
                    item.sendMessage(message);
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketServer.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketServer.onlineCount--;
    }
}

package cn.com.websocket.vo;

public class MessageVO {

    /**
     * 来源
     */
    private String fromSid;

    /**
     * 发送的对象
     */
    private String toSid;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 消息类型：1代表上线 2代表下线 3代表在线名单 4代表普通消息
     */
    private Integer messageType;

    public String getFromSid() {
        return fromSid;
    }

    public void setFromSid(String fromSid) {
        this.fromSid = fromSid;
    }

    public String getToSid() {
        return toSid;
    }

    public void setToSid(String toSid) {
        this.toSid = toSid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getMessageType() {
        return messageType;
    }

    public void setMessageType(Integer messageType) {
        this.messageType = messageType;
    }
}
